# Multifunkcionális Express.js szerver

## Rövid leírás:
Ez egy Express.js-re épülő node.js szerver, ami egyszerűen kezelhető modul-rendszerrel és különböző alap funkciókkal lett kiegészítve.

## Telepítés / Indítás

Bővebben a `devel/readme.md` -ben

## Eddigi modulok
### qmining
Backend és weboldal a [Moodle/Kmooc teszt megoldó kliens](https://gitlab.com/MrFry/moodle-test-userscript)hez. A `./public` mappában található `data.json` fájlból tölti be az összes kérdést, és szolgálja ki a klienseket. Beállított időnként ebbe menti az új kérdéseket is, és a `./public/backs` mappába másol biztonsági mentéseket. Főoldalán `./public/qa` fájlból jeleníti meg a felhasználók kérdéseit, és az üzemeltető válaszait, amit manuálisan kell szerkeszteni.

Fontosabb oldalak:

név | leírás
--- | ---
/legacy| Összes kérdés/válasz egy oldalon
/isAdding| Erre a címre POST-olja a kliens az új kérdéseket
/ask | Ezt a címet kéri le paraméterezve a kliens ha kérdésre keres

### stuff
Egyszerű fájlböngésző, ami a `./public/files` mappa tartalmát listázza ki böngészőben

### sio
Egyszerű oldal a szerverre való fájlfeltöltés megkönnyítésére

### main
Főoldal / minta modul

# Üzemelés

## Új modul létrehozása
Javasol a 'main' modul másolás, és átnevezése a `./modules` mappában, abban szinte minden alapvető funkció benne van. Majd a `./modules.json` fájlba egy új bejegyzést kell létrehozni a többi alapján. Ezt a `./extraModules` fájlban is meg lehet tenni, ami csak azért létezik hogy privát modulokat ne kelljen git-re feltölteni.

A szerver `vhost` csomagot használ, és több aldomainje van, amikre érdemes figyelni

## ./stats mappa
Ebben található az összes statisztika és naplófájl

név | leírás
--- | --- 
./stats/logs | részletes request napló
./stats/nlogs | fontosabb request napló
./stats/stats | összes lekért oldal JSON
./stats/vstats | napokba rendezett összes lekérd oldal JSON
./stats/idstats | Összes kérdés hozzáadó kliens egyedi azonosító statisztika JSON
./stats/idvstats | Összes kérdés hozzáadó kliens egyedi azonosító napokba rendezve JSON

## ./utils mappa
Különböző hasznos eszközök

név | leírás
--- | ---
logger.js | minden naplózást kezel
dbcheck.js | paraméterként kapott adatbázist ellenőrzi, hogy van-e a kérdéseknek `.Q` propertyje, ami a régi fajta módszernél volt használatos
actions.js | qmining modul beérkező kérdés feldolgozás
utils.js | alapvető eszközök, pl fájl beolvasás
motd.js | `data.json` és ./public/motd -be írja a paraméterként kapott szöveget
ids.js | egyedi felhasználó azonosítókat írja statisztika fájlba
dataUpdater.js | régifajta adatbázist, amiben még van `.Q` propertyjű kérdés alakít át
changedataversion.js | `data.json`-ban és a ./public/version ban írja át a teszt megoldó kliens aktuális verzióját
merge.sh | Biztonsági mentést készít, és egyszerűsíti az adatbázist, majd felülírja az újjal
classes.js | Összehasonlításhoz és tároláshoz szükséges osztályok
dbSetup.js | Üres / előre userrel feltöltött adatbázist hoz létre
dbtools.js | Javascript wrapper gyakran használt SQL utasításokhoz
rmDuplicates.js | Paraméterként átadott JSON kérdés adatbázisból távolítja el az ugyanolyan kérdéseket
runSqliteCmds.sh | Paraméterként átadott adatbázison futtatja a második paraméterben található Sqlite parancsokat

# Egyéb
Jelenleg sok optimalizálatlan rész található benne, cél ezek kijavítása, szépítése

# Licensz:
GPLv3
