/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

// package requires
const express = require('express')
const bodyParser = require('body-parser')
const busboy = require('connect-busboy')
const fs = require('fs')
const app = express()

// other requires
const logger = require('../../utils/logger.js')
const utils = require('../../utils/utils.js')

// stuff gotten from server.js
let publicdirs = []

function GetApp () {
  const p = publicdirs[0]
  if (!p) {
    throw new Error(`No public dir! ( SIO )`)
  }

  // files in public dirs
  const uloadFiles = p + 'f'

  app.set('view engine', 'ejs')
  app.set('views', [
    './modules/sio/views',
    './sharedViews'
  ])
  publicdirs.forEach((pdir) => {
    logger.Log(`Using public dir: ${pdir}`)
    app.use(express.static(pdir))
  })
  app.use(busboy({
    limits: {
      fileSize: 10000 * 1024 * 1024
    }
  }))
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({
    limit: '5mb',
    extended: true
  }))
  app.use(bodyParser.json({
    limit: '5mb'
  }))

  // --------------------------------------------------------------

  app.get('/', function (req, res) {
    res.render('uload')
    res.end()
  })

  function UploadFile (req, res, path, next) {
    var fstream
    req.pipe(req.busboy)
    req.busboy.on('file', function (fieldname, file, filename) {
      logger.Log('Uploading: ' + filename, logger.GetColor('blue'))

      utils.CreatePath(path, true)
      let d = new Date()
      let fn = d.getHours() + '' + d.getMinutes() + '' + d.getSeconds() + '_' + filename

      fstream = fs.createWriteStream(path + '/' + fn)
      file.pipe(fstream)
      fstream.on('close', function () {
        logger.Log('Upload Finished of ' + path + '/' + fn, logger.GetColor('blue'))
        next(fn)
      })
      fstream.on('error', function (err) {
        console.log(err)
        res.end('something bad happened :s')
      })
    })
  }

  app.route('/fosuploader').post(function (req, res, next) {
    UploadFile(req, res, uloadFiles, (fn) => {
      res.redirect('/f/' + fn)
    })
  })
  app.get('*', function (req, res) {
    res.status(404).render('404')
  })

  app.post('*', function (req, res) {
    res.status(404).render('404')
  })

  return {
    app: app
  }
}

exports.name = 'Sio'
exports.getApp = GetApp
exports.setup = (data) => {
  publicdirs = data.publicdirs
}
