/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

// package requires
const express = require('express')
const bodyParser = require('body-parser')
const busboy = require('connect-busboy')
const app = express()

// other requires
const logger = require('../../utils/logger.js')

// stuff gotten from server.js
let publicdirs = []
let url = '' // http(s)//asd.basd

function GetApp () {
  app.set('view engine', 'ejs')
  app.set('views', [
    './modules/main/views',
    './sharedViews'
  ])
  publicdirs.forEach((pdir) => {
    logger.Log(`Using public dir: ${pdir}`)
    app.use(express.static(pdir))
  })
  app.use(busboy({
    limits: {
      fileSize: 10000 * 1024 * 1024
    }
  }))
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({
    limit: '5mb',
    extended: true
  }))
  app.use(bodyParser.json({
    limit: '5mb'
  }))

  // --------------------------------------------------------------

  app.get('/', function (req, res) {
    res.render('main', {
      siteurl: url
    })
  })

  app.get('*', function (req, res) {
    res.status(404).render('404')
  })

  app.post('*', function (req, res) {
    res.status(404).render('404')
  })

  return {
    app: app
  }
}

exports.name = 'Main'
exports.getApp = GetApp
exports.setup = (data) => {
  url = data.url
  publicdirs = data.publicdirs
}
