/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

// package requires
const express = require('express')
const bodyParser = require('body-parser')
const busboy = require('connect-busboy')
const app = express()

// other requires
const utils = require('../../utils/utils.js')
const logger = require('../../utils/logger.js')
const auth = require('../../middlewares/auth.middleware.js')

// stuff gotten from server.js
let donateURL = ''
let publicdirs = []
let userDB
let nextdir = ''

try {
  donateURL = utils.ReadFile('./data/donateURL')
} catch (e) {
  logger.Log('Couldnt read donate URL file!', logger.GetColor('red'))
}

function GetApp () {
  app.use(bodyParser.urlencoded({
    limit: '5mb',
    extended: true
  }))
  app.use(bodyParser.json({
    limit: '5mb'
  }))
  app.set('view engine', 'ejs')
  app.set('views', [
    './modules/qmining/views',
    './sharedViews'
  ])
  app.use(auth({
    userDB: userDB,
    jsonResponse: false,
    exceptions: [
      '/thanks',
      '/thanks.html',
      '/img/thanks.gif',
      '/install',
      '/favicon.ico',
      '/getVeteranPw',
      '/moodle-test-userscript/stable.user.js',
      '/donate',
      '/irc'
    ]
  }))
  publicdirs.forEach((pdir) => {
    logger.Log(`Using public dir: ${pdir}`)
    app.use(express.static(pdir))
  })
  app.use(express.static(nextdir))
  app.use(busboy({
    limits: {
      fileSize: 10000 * 1024 * 1024
    }
  }))

  // --------------------------------------------------------------
  // REDIRECTS
  // --------------------------------------------------------------

  // to be backwards compatible
  app.get('/ask', function (req, res) {
    logger.DebugLog(`Qmining module ask redirect`, 'ask', 1)
    res.redirect(`http://api.frylabs.net/ask?q=${req.query.q}&subj=${req.query.subj}&data=${req.query.data}`)
  })

  const simpleRedirects = [
    {
      from: '/dataeditor',
      to: 'https://dataeditor.frylabs.net'
    },
    {
      from: '/install',
      to: 'https://qmining.frylabs.net/moodle-test-userscript/stable.user.js'
    },
    {
      from: '/servergit',
      to: 'https://gitlab.com/MrFry/mrfrys-node-server'
    },
    {
      from: '/scriptgit',
      to: 'https://gitlab.com/MrFry/moodle-test-userscript'
    },
    {
      from: '/qminingSite',
      to: 'https://gitlab.com/MrFry/qmining-page'
    },
    {
      from: '/classesgit',
      to: 'https://gitlab.com/MrFry/question-classes'
    },
    {
      from: '/menuClick',
      to: '/'
    },
    {
      from: '/lred',
      to: '/allQuestions.html'
    },
    {
      from: '/donate',
      to: donateURL
    },
    { // to be backwards compatible
      from: '/legacy',
      to: '/allQuestions.html'
    },
    {
      from: '/allqr',
      to: 'http://api.frylabs.net/allqr.txt'
    },
    {
      from: '/allqr.txt',
      to: 'http://api.frylabs.net/allqr.txt'
    },
    {
      from: '/infos',
      to: 'http://api.frylabs.net/infos?version=true&motd=true&subjinfo=true',
      nolog: true
    },
    {
      from: '/irc',
      to: 'https://kiwiirc.com/nextclient/irc.sub.fm/#qmining'
    }
  ]

  simpleRedirects.forEach((redirect) => {
    app.get(redirect.from, function (req, res) {
      if (!redirect.nolog) {
        logger.LogReq(req)
      }
      logger.DebugLog(`Qmining module ${redirect.from} redirect`, 'infos', 1)
      res.redirect(`${redirect.to}`)
    })
  })

  // --------------------------------------------------------------

  function AddHtmlRoutes (files) {
    const routes = files.reduce((acc, f) => {
      if (f.includes('html')) {
        acc.push(f.split('.')[0])
        return acc
      }
      return acc
    }, [])

    routes.forEach((route) => {
      logger.DebugLog(`Added route /${route}`, 'Qmining routes', 1)
      app.get(`/${route}`, function (req, res) {
        logger.LogReq(req)
        res.redirect(`${route}.html`)
      })
    })
  }
  AddHtmlRoutes(utils.ReadDir(nextdir))

  // --------------------------------------------------------------

  app.get('/', function (req, res) {
    res.end('hai')
    logger.LogReq(req)
  })

  app.get('/getVeteranPw', function (req, res) {
    res.render('veteranPw', {
      cid: req.query.cid || '',
      devel: process.env.NS_DEVEL
    })
    logger.LogReq(req)
  })

  app.get('*', function (req, res) {
    res.status(404).render('404')
  })

  app.post('*', function (req, res) {
    res.status(404).render('404')
  })

  return {
    app: app
  }
}

exports.name = 'Qmining'
exports.getApp = GetApp
exports.setup = (data) => {
  userDB = data.userDB
  publicdirs = data.publicdirs
  nextdir = data.nextdir
}
