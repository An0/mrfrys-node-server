/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

// package requires
const express = require('express')
const bodyParser = require('body-parser')
const busboy = require('connect-busboy')
const app = express()

// other requires
const utils = require('../../utils/utils.js')
const logger = require('../../utils/logger.js')
const auth = require('../../middlewares/auth.middleware.js')

// stuff gotten from server.js
let userDB
let publicdirs = []
let nextdir = ''

function GetApp () {
  app.use(bodyParser.urlencoded({
    limit: '5mb',
    extended: true
  }))
  app.use(bodyParser.json({
    limit: '5mb'
  }))
  app.set('view engine', 'ejs')
  app.set('views', [
    './modules/dataEditor/views',
    './sharedViews'
  ])
  app.use(auth({
    userDB: userDB,
    jsonResponse: false,
    exceptions: [
      '/favicon.ico',
      '/getVeteranPw'
    ]
  }))
  publicdirs.forEach((pdir) => {
    logger.Log(`Using public dir: ${pdir}`)
    app.use(express.static(pdir))
  })
  app.use(express.static(nextdir))
  app.use(busboy({
    limits: {
      fileSize: 10000 * 1024 * 1024
    }
  }))

  // --------------------------------------------------------------

  function AddHtmlRoutes (files) {
    const routes = files.reduce((acc, f) => {
      if (f.includes('html')) {
        acc.push(f.split('.')[0])
        return acc
      }
      return acc
    }, [])

    routes.forEach((route) => {
      logger.DebugLog(`Added route /${route}`, 'DataEditor routes', 1)
      app.get(`/${route}`, function (req, res) {
        logger.LogReq(req)
        res.redirect(`${route}.html`)
      })
    })
  }
  AddHtmlRoutes(utils.ReadDir(nextdir))

  // --------------------------------------------------------------

  app.get('/', function (req, res) {
    res.end('hai')
    logger.LogReq(req)
  })

  app.get('*', function (req, res) {
    res.status(404).render('404')
  })

  app.post('*', function (req, res) {
    res.status(404).render('404')
  })

  return {
    app: app
  }
}

exports.name = 'Data editor'
exports.getApp = GetApp
exports.setup = (data) => {
  userDB = data.userDB
  publicdirs = data.publicdirs
  nextdir = data.nextdir
}
