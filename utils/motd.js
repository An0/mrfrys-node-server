const utils = require('../utils/utils.js')
const dataFile = '../public/data.json'
const motdFile = '../public/motd'

var p = GetParams()
if (p.length <= 0) {
  console.log('no params!')
  process.exit(0)
}

var param = p.join(' ')
console.log('param: ' + param)
var d = utils.ReadFile(dataFile)
var parsed = JSON.parse(d)
parsed.motd = param
utils.WriteFile(JSON.stringify(parsed), dataFile)

utils.WriteFile(parsed.motd, motdFile)

function GetParams () {
  return process.argv.splice(2)
}
