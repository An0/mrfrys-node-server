# Contents

Utils mappa tartalom

## merger.js

Törölve lesz

## merge.sh

Törölve lesz

## rmDuplicates.js

Tervezve

Kitörli a két vagy többször szereplő kérdéseket

Params:

1. Elérési út a JSON adatbázishoz

## actions.js

Át lesz nevezve: `questionProcessor.js`

JSON adatbázist betölti, és az új bejövő kérdéseket hozzáadja

Exportált funkciók:

1. ProcessIncomingRequest
2. LoadJSON

## utils.js

Általános eszközök

Exportált funkciók:

1. ReadFile
2. WriteFile
3. writeFileAsync
4. AppendToFile
5. Beep
6. WriteBackup
7. FileExists
8. CreatePath
9. WatchFile
10. ReadDir

## logger.js

Logolást kezeli

Exportált funkciók:

1. GetDateString
2. Log
3. DebugLog
4. GetColor
5. LogReq
6. LogStat
7. Load
8. logHashed
9. hr

## motd.js

Az adatbázis MOTD-ét változtatja meg. Elvileg már nem kell

`node motd.js "new motd"`

## changedataversion.js

Az adatbázis jelenlegi legfrissebb kliens verzióját változtatja meg. Elvileg már nem kell

`node motd.js "new version"`

## ids.js

`./stats/idstats` és `./stats/idvstats` ba írja a egyedi kliens ID statisztikákat

Exportált funkciók:

1. LogId
2. Load
