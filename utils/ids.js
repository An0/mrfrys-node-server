/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

module.exports = {
  LogId: LogId,
  Load: Load
}

const utils = require('../utils/utils.js')
const logger = require('../utils/logger.js')
const idStatFile = 'stats/idstats'
const idVStatFile = 'stats/idvstats'

const writeInterval = 1

let data = {}
let vData = {}
let writes = 0

function Load () {
  try {
    var prevData = utils.ReadFile(idStatFile)
    data = JSON.parse(prevData)
  } catch (e) {
    logger.Log('Error at loading id logs! (@ first run its normal)', logger.GetColor('redbg'))
    console.log(e)
  }

  try {
    var prevVData = utils.ReadFile(idVStatFile)
    vData = JSON.parse(prevVData)
  } catch (e) {
    logger.Log('Error at loading id logs! (@ first run its normal)', logger.GetColor('redbg'))
    console.log(e)
  }
}

function LogId (id, subj) {
  Inc(id, subj)
  AddVisitStat(id, subj)
  Save()
}

function AddSubjToList (list, subj) {
  if (!list[subj]) {
    list[subj] = 0
  }
  list[subj]++
}

function Inc (value, subj) {
  if (data[value] === undefined) {
    data[value] = {
      count: 0,
      subjs: {}
    }
  }
  data[value].count++
  AddSubjToList(data[value].subjs, subj)
}

function AddVisitStat (name, subj) {
  var m = new Date()
  const now = m.getFullYear() + '/' + ('0' + (m.getMonth() + 1)).slice(-2) + '/' + ('0' + m.getDate()).slice(-2)
  if (vData[now] === undefined) { vData[now] = {} }
  if (vData[now][name] === undefined) {
    vData[now][name] = {
      count: 0,
      subjs: {}
    }
  }
  vData[now][name].count++
  AddSubjToList(vData[now][name].subjs, subj)
}

function Save () {
  writes++
  if (writes === writeInterval) {
    try {
      utils.WriteFile(JSON.stringify(data), idStatFile)
      // Log("Stats wrote.");
    } catch (e) {
      logger.Log('Error at writing logs!', logger.GetColor('redbg'))
      console.log(e)
    }
    try {
      utils.WriteFile(JSON.stringify(vData), idVStatFile)
      // Log("Stats wrote.");
    } catch (e) {
      logger.Log('Error at writing visit logs!', logger.GetColor('redbg'))
      console.log(e)
    }
    writes = 0
  }
}
