const utils = require('../utils/utils.js')
const logger = require('../utils/logger.js')
const dbtools = require('../utils/dbtools.js')
const dbStructPath = '../modules/api/apiDBStruct.json'
const usersDBPath = '../data/dbs/users.db'
const uuidv4 = require('uuid/v4') // TODO: deprecated, but imports are not supported

let authDB

console.clear()
CreateDB()

authDB.close()

function CreateDB () {
  const dbStruct = utils.ReadJSON(dbStructPath)
  // authDB = dbtools.GetDB(':memory:')
  authDB = dbtools.GetDB(usersDBPath)
  authDB.pragma('synchronous = OFF')

  Object.keys(dbStruct).forEach((tableName) => {
    const tableData = dbStruct[tableName]
    dbtools.CreateTable(authDB, tableName, tableData.tableStruct, tableData.foreignKey)
  })

  try {
    if (utils.FileExists('./ids')) {
      const uids = utils.ReadFile('./ids').split('\n')

      uids.forEach((cid, i) => {
        if (!cid) { return }
        logger.Log(`[ ${i} / ${uids.length} ]`)
        try {
          dbtools.Insert(authDB, 'users', {
            pw: uuidv4(),
            oldCID: cid,
            avaiblePWRequests: 4,
            created: utils.GetDateString()
          })
        } catch (e) {
          logger.Log('Error during inserting', logger.GetColor('redbg'))
          console.error(e)
        }
      })
    }
  } catch (e) {
    console.error(e)
  }

  const dir = `./dbSetupResult/${utils.GetDateString().replace(/ /g, '_')}`
  utils.CreatePath(dir)
  Object.keys(dbStruct).forEach((key) => {
    const path = `${dir}/${key}.json`
    logger.Log(`Writing ${path}...`)
    utils.WriteFile(JSON.stringify({
      tableInfo: dbtools.TableInfo(authDB, key),
      tableRows: dbtools.SelectAll(authDB, key)
    }), path)
  })

  logger.Log('Done')
}
