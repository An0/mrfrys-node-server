/* ----------------------------------------------------------------------------
Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

module.exports = {
  ProcessIncomingRequest: ProcessIncomingRequest,
  LoadJSON: LoadJSON
}

const dataFile = './qminingPublic/data.json'
const recDataFile = './stats/recdata'

const logger = require('../utils/logger.js')
const idStats = require('../utils/ids.js')
idStats.Load() // FIXME: dont always load when actions.js is used
const utils = require('../utils/utils.js')
const classes = require('./classes.js')
classes.initLogger(logger.DebugLog)
// if a recievend question doesnt match at least this % to any other question in the db it gets
// added to db
const minMatchAmmountToAdd = 90 // FIXME: test this value

const writeAfter = 1 // write after # of adds FIXME: set reasonable save rate
var currWrites = 0

function ProcessIncomingRequest (recievedData, qdb, infos, dryRun) {
  logger.DebugLog('Processing incoming request', 'actions', 1)
  if (recievedData === undefined) {
    logger.Log('\tRecieved data is undefined!', logger.GetColor('redbg'))
    return
  }

  try {
    let towrite = logger.GetDateString() + '\n'
    towrite += '------------------------------------------------------------------------------\n'
    if (typeof recievedData === 'object') {
      towrite += JSON.stringify(recievedData)
    } else {
      towrite += recievedData
    }
    towrite += '\n------------------------------------------------------------------------------\n'
    utils.AppendToFile(towrite, recDataFile)
    logger.DebugLog('recDataFile written', 'actions', 1)
  } catch (e) {
    logger.log('Error writing recieved data.')
  }

  try {
    // recievedData: { version: "", id: "", subj: "" quiz: {} }
    let d = recievedData
    // FIXME: if is for backwards compatibility, remove this sometime in the future
    if (typeof d !== 'object') {
      d = JSON.parse(recievedData)
    }

    logger.DebugLog('recievedData JSON  parsed', 'actions', 1)
    logger.DebugLog(d, 'actions', 3)
    let allQLength = d.quiz.length
    let allQuestions = []

    d.quiz.forEach((question) => {
      logger.DebugLog('Question:', 'actions', 2)
      logger.DebugLog(question, 'actions', 2)
      let q = new classes.Question(question.Q, question.A, question.data)
      logger.DebugLog('Searching for question in subj ' + d.subj, 'actions', 3)
      logger.DebugLog(q, 'actions', 3)

      let sames = qdb.Search(q, d.subj)
      logger.DebugLog('Same questions:', 'actions', 2)
      logger.DebugLog('Length: ' + sames.length, 'actions', 2)
      logger.DebugLog(sames, 'actions', 3)
      // if it didnt find any question, or every found questions match is lower thatn 80
      let isNew = sames.length === 0 || sames.every(searchResItem => {
        return searchResItem.match < minMatchAmmountToAdd
      })
      logger.DebugLog('isNew: ' + isNew, 'actions', 2)
      if (isNew) {
        allQuestions.push(q)
      }
    })

    let color = logger.GetColor('green')
    let msg = ''
    if (allQuestions.length > 0) {
      color = logger.GetColor('blue')
      msg += `New questions: ${allQuestions.length} ( All: ${allQLength} )`
      allQuestions.forEach((q) => {
        const sName = classes.SUtils.GetSubjNameWithoutYear(d.subj)
        logger.DebugLog('Adding question with subjName: ' + sName + ' :', 'actions', 3)
        logger.DebugLog(q, 'actions', 3)
        qdb.AddQuestion(sName, q)
      })

      currWrites++
      logger.DebugLog('currWrites for data.json: ' + currWrites, 'actions', 1)
      if (currWrites >= writeAfter && !dryRun) {
        currWrites = 0
        try {
          qdb.version = infos.version
          qdb.motd = infos.motd
          logger.DebugLog('version and motd set for data.json', 'actions', 3)
        } catch (e) {
          logger.Log('MOTD/Version writing/reading error!')
        }
        logger.DebugLog('Writing data.json', 'actions', 1)
        utils.WriteFile(JSON.stringify(qdb), dataFile)
        logger.Log('\tData file written', color)
      } else if (dryRun) {
        logger.Log('\tDry run')
      }
    } else {
      msg += `No new data ( ${allQLength} )`
    }

    let subjRow = '\t' + d.subj
    if (d.id) {
      subjRow += ' ( CID: ' + logger.logHashed(d.id) + ')'
      idStats.LogId(d.id, d.subj)
    }
    logger.Log(subjRow)
    if (d.version !== undefined) { msg += '. Version: ' + d.version }

    logger.Log('\t' + msg, color)
    logger.DebugLog('New Questions:', 'actions', 2)
    logger.DebugLog(allQuestions, 'actions', 2)

    logger.DebugLog('ProcessIncomingRequest done', 'actions', 1)
    return allQuestions.length
  } catch (e) {
    console.log(e)
    logger.Log('Couldnt parse JSON data', logger.GetColor('redbg'))
    return -1
  }
}

// loading stuff
function LoadJSON (dataFile) {
  try {
    var d = JSON.parse(utils.ReadFile(dataFile))
    var r = new classes.QuestionDB()
    var rt = []

    for (var i = 0; i < d.Subjects.length; i++) {
      let s = new classes.Subject(d.Subjects[i].Name)
      var j = 0
      for (j = 0; j < d.Subjects[i].Questions.length; j++) {
        var currQ = d.Subjects[i].Questions[j]
        s.AddQuestion(new classes.Question(currQ.Q, currQ.A, currQ.data))
      }
      rt.push({
        name: d.Subjects[i].Name,
        count: j
      })
      r.AddSubject(s)
    }
    return r
  } catch (e) {
    logger.Log('Error loading sutff', logger.GetColor('redbg'), true)
    console.log(e)
  }
}
