const utils = require('../utils/utils.js')
const dataFile = '../public/data.json'
const versionFile = '../public/version'

var p = GetParams()
if (p.length <= 0) {
  console.log('no params!')
  process.exit(0)
}

var param = p.join(' ')

console.log('param: ' + param)

var d = utils.ReadFile(dataFile)
var parsed = JSON.parse(d)

console.log('Old version:')
console.log(parsed.version)

parsed.version = param

console.log('New version:')
console.log(parsed.version)

utils.WriteFile(JSON.stringify(parsed), dataFile)
utils.WriteFile(parsed.version, versionFile)

function GetParams () {
  return process.argv.splice(2)
}
