module.exports = {
  ReadFile: ReadFile,
  ReadJSON: ReadJSON,
  WriteFile: WriteFile,
  writeFileAsync: WriteFileAsync,
  AppendToFile: AppendToFile,
  Beep: Beep,
  WriteBackup: WriteBackup,
  FileExists: FileExists,
  CreatePath: CreatePath,
  WatchFile: WatchFile,
  ReadDir: ReadDir,
  CopyFile: CopyFile,
  GetDateString: GetDateString
}

var fs = require('fs')

var logger = require('../utils/logger.js')

const dataFile = './qminingPublic/data.json'

function GetDateString () {
  const m = new Date()
  return m.getFullYear() + '-' +
    ('0' + (m.getMonth() + 1)).slice(-2) + '-' +
    ('0' + m.getDate()).slice(-2) + ' ' +
    ('0' + m.getHours()).slice(-2) + ':' +
    ('0' + m.getMinutes()).slice(-2) + ':' +
    ('0' + m.getSeconds()).slice(-2)
}

function CopyFile (from, to) {
  CreatePath(to)
  fs.copyFileSync(from, to)
}

function ReadDir (path) {
  return fs.readdirSync(path)
}

function ReadJSON (name) {
  try {
    return JSON.parse(ReadFile(name))
  } catch (e) {
    console.log(e)
    throw new Error('Coulndt parse JSON in "ReadJSON", file: ' + name)
  }
}

function ReadFile (name) {
  if (!FileExists(name)) { throw new Error('No such file: ' + name) }
  return fs.readFileSync(name, 'utf8')
}

function FileExists (path) {
  return fs.existsSync(path)
}

function WatchFile (file, callback) {
  if (FileExists(file)) {
    fs.watchFile(file, (curr, prev) => {
      fs.readFile(file, 'utf8', (err, data) => {
        if (err) {
          // console.log(err)
        } else {
          callback(data)
        }
      })
    })
  } else {
    console.log(file + ' does not eadjsalék')
    setTimeout(() => {
      WatchFile(file)
    }, 1000)
  }
}

function CreatePath (path, onlyPath) {
  if (FileExists(path)) { return }

  var p = path.split('/')
  var currDir = p[0]
  for (var i = 1; i < p.length; i++) {
    if (currDir !== '' && !fs.existsSync(currDir)) {
      try {
        fs.mkdirSync(currDir)
      } catch (e) { console.log('Failed to make ' + currDir + ' directory... ') }
    }
    currDir += '/' + p[i]
  }
  if (onlyPath === undefined || onlyPath === false) {
  } else {
    fs.mkdirSync(path)
  }
}

function WriteFile (content, path) {
  CreatePath(path)
  fs.writeFileSync(path, content)
}

function WriteFileAsync (content, path) {
  CreatePath(path)
  fs.writeFile(path, content, function (err) {
    if (err) {
      logger.Log('Error writing file: ' + path + ' (sync)', logger.GetColor('redbg'))
    }
  })
}

function AppendToFile (data, file) {
  CreatePath(file)
  try {
    fs.appendFileSync(file, '\n' + data)
  } catch (e) {
    logger.Log('Error appendig to file log file: ' + file + ' (sync)', logger.GetColor('redbg'))
    logger.Log(data)
    console.log(e)
  }
}

function Beep () {
  try {
    process.stdout.write('\x07')
  } catch (e) {
    console.log('error beepin')
  }
}

function WriteBackup () {
  try {
    WriteFileAsync(ReadFile(dataFile), 'public/backs/data_' + new Date().toString())
  } catch (e) {
    logger.Log('Error backing up data json file!', logger.GetColor('redbg'))
    console.log(e)
  }
}
