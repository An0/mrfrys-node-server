# Setup

## Linux

./make.sh

## Windows

Install linux

just kidding, windowson még sosem próbáltam, ha valaki rájön ott hogy működik, akkor az jól jönne, ty.

A `make.sh` script futtatása minden eddigi szerver által használt/generált adatot felülír!

# Futtatás

Szükséges könyvtár struktúrát és egyéb fájlokat automatikusan létrehozza a `make.sh` script

Majd:

`npm run dev` 
vagy 
`npm start` 

## Környezeti változók
* `NS_SQL_DEBUG_LOG`
   
   Ha `true`, akkor minden SQL utasítás ki lesz írva a konzolra

* `NS_LOGLEVEL`

  Egy szám lehet, ami minél nagyobb annál részletesebben történik a logolás konzolra. Jelenleg ez
  ilyen 1-4 közötti skálán mozog.

* `NS_DEVEL`

  'Devel' ként futtatja a szervert. Jelenleg ez azt jelenti, hogy pár helyen https helyett http-re
  redirectel.

## Adatbázis előkészítése

A szerver egy felhasználókat, session-öket és ehhez tartozó segéd táblákat tartalmazó adatbázist
használ. Az adatbázis struktúrát a `modules/api/apiDBStruct.json` tartalmazza, és az adatbázis
módosításához szükséges függvények az `utils/dbtools.js` fájlban vannak megvalósítva. Így nem kell
SQL lekérdezéseket írni, csak ezeket meghívni.

A `make.sh` script automatikusan létrehoz egy üres adatbázist a `utils/dbSetup.js` segítségével.
Később ezt manuálisan futtatva alaphelyzetbe lehet állítani az adatbázist. Ha létezik egy
`utils/ids` fájl, ami sorokban felhasználó kliens ID-kat tartalmaz, akkor a `utils/dbSetup.js`
automatikusan létrehoz ezek alapján felhasználókat.

## Hogy az API és a többi modul tudjon kommunikálni:

__Ezt a rész csak nagyon kevés esetben kell megcsinálni, ajánlott kihagyni! Ennélkül is működik
lokálisan az API és a usercript!__

1. Ezt a pár sort add hozzá a `/etc/hosts` fájlhoz:
   
   ```
   127.0.0.1   api.frylabs.net
   127.0.0.1   qmining.frylabs.net
   ```

   Figyelj rá, hogy az IP cím és az url közötti spacing az pontosan 1 tab!

   Firefox nem mindig használja a hosts filet! Ennélkül a qmining modul nem bír kommunikálni az api-val, ahonnan szedi a kérdéseket, motd-t, felhasználó kérdéseket és ilyesmiket.

2. Ezután a server.js-ben a portot írd át 80-ra és superuserként kell indítani a szervert

3. Ezután a böngészőben a qmining.frylabs.net-en a gépen futó szerver lesz elérhető

   A frylabs.net-es url-ek helyett lehet mást is használni, de néhány helyen előfordulhat hogy be van még égetve, ezért ezek ajánlottak

4. Ha végeztél ne felejtsd el kitörölni a `hosts` fájlból a bemásolt sorokat, mert annélkül nem lesz elérhető az eredeti szerver!

# stats mappa

### logs

Szuper részletes log, minden requestről, ajánlatos egy `tail -f stats/logs`-al nézni mi
történik

### msgs

A feedback oldalra írt üzenetek ebbe mentődnek

### stats

JSON file, kulcsok az url-ek, az értékek pedig hogy hányszor lettek lekérve

### vstats

Ugyanaz mint a `stats` file, csak napokba rendezve

### idstats

Cliens ID összes statisztika

### idvstats

Cliens ID összes statisztika napokba rendezve

### recdata

Az az adat, amit a szerver az `/isadding` végpontra kap

### dataEdits

Néhány fontos log amit az api generál mikor a felhasználók a dataEditor modult használják
