# Teszt scriptek a API/qmining modulhoz

A `./devel/tests/serverAddress` fájl első sora a tesztelendő szerver címének kell lennie! A szerver
alap címe: `localhost:8080`

# Mappák

## testScripts

Ebben a mappában alap API végpont tesztelő scriptek vannak.

### ask

`ask.sh`: kérdést tesz fel a szervernek, szintaxis:

`./ask.sh 'kérdés' 'tárgy neve'`
vagy
`./ask.sh 'kérdés' 'kérdés adat JSON' 'kérdés data JSON'`

### postTestData

`postTestData.sh`: szervernek elküld JSON adatot amit hozzá akarunk adatni a kérdés adatbázishoz

`./postTestData.sh 'JSON adat'`
vagy (bash-ben):
`./postTestData.sh $(cat 'JSON file elérési út')`

## complexTestScripts

Ebben a mappában komplexebb teszt scriptek vannak, amik az előző API hívásokat kombinálják. A
scripteknek nincsenek paraméterei, és elején pár `echo`-nak el kellene illedelmesen mondania hogy
mit tesztel, és mi az expected eredmény.

## bin

Pár random script amit minden használhat. Pl `hr.sh`: horizontal ruler

## testData

JSON-ok, amiben szervernek küldhető teszt adat van

## rawTestData

Egyéb teszt adat, pl több JSON egy fájlban, meg ilyesmi
