#!/bin/bash
url=$(head -n 1 ../serverAddress)
echo "Server url: $url"


if [ "$#" -ne 1 ]; then
  echo "1 param required: json to send to localhost, got $#"
else
  data=$(tr -d '\n' < "$1" | awk '$1=$1')
  echo sending
  echo "SENT DATA:"
  ../bin/hr.sh
  echo "$data"
  ../bin/hr.sh
  curl -X POST --data "datatoadd=$data" "$url/isAdding"
fi

