const logger = require('../utils/logger.js')

module.exports = function (options) {
  const loggableKeywords = options ? options.loggableKeywords : undefined
  const loggableModules = options ? options.loggableModules : undefined

  return function (req, res, next) {
    res.on('finish', function () {
      if (req.url.includes('_next/static')) {
        return
      }

      const ip = req.headers['cf-connecting-ip'] || req.connection.remoteAddress
      let hostname = 'NOHOST'
      if (req.hostname) {
        hostname = req.hostname.replace('www.', '').split('.')[0]
      } else {
        logger.Log('Hostname is undefined!', logger.GetColor('redbg'))
        console.log(req.body)
        console.log(req.query)
        console.log(req.headers)
      }

      // fixme: regexp includes checking
      const hasLoggableKeyword = loggableKeywords && loggableKeywords.some((x) => {
        return req.url.includes(x)
      })
      const hasLoggableModule = loggableModules && loggableModules.some((x) => {
        return hostname.includes(x)
      })
      const toLog = hasLoggableModule || hasLoggableKeyword

      logger.LogReq(req, true, res.statusCode)
      if (toLog) { logger.LogReq(req) }
      if (res.statusCode !== 404) {
        logger.LogStat(req.url,
          ip,
          hostname,
          req.session && req.session.user ? req.session.user.id : 'NOUSER'
        )
      }
    })
    next()
  }
}
