const logger = require('../utils/logger.js')
const utils = require('../utils/utils.js')
const dbtools = require('../utils/dbtools.js')

module.exports = function (options) {
  const { userDB, jsonResponse, exceptions } = options

  const renderLogin = (req, res) => {
    res.status('401') // Unauthorized
    if (jsonResponse) {
      res.json({
        result: 'nouser',
        msg: 'You are not logged in'
      })
    } else {
      res.render('login', {
        devel: process.env.NS_DEVEL
      })
    }
  }

  return function (req, res, next) {
    const ip = req.headers['cf-connecting-ip'] || req.connection.remoteAddress
    const sessionID = req.cookies.sessionID
    const isException = exceptions.some((exc) => {
      return req.url.split('?')[0] === exc
    })

    if (process.env.NS_NOUSER) {
      req.session = {
        user: {
          id: 21323
        },
        sessionID: sessionID || 111111111111111111,
        isException: false
      }
      next()
      return
    }

    // FIXME Allowing all urls with _next in it, but not in params
    if (req.url.split('?')[0].includes('_next') || req.url.split('?')[0].includes('well-known/acme-challenge')) {
      req.session = { isException: true }
      next()
      return
    }

    if (!sessionID) {
      if (isException) {
        logger.DebugLog(`EXCEPTION: ${req.url}`, 'auth', 1)
        req.session = { isException: true }
        next()
        return
      }
      logger.DebugLog(`No session ID: ${req.url}`, 'auth', 1)
      renderLogin(req, res)
      return
    }

    const user = GetUserBySessionID(userDB, sessionID, req)

    if (!user) {
      if (isException) {
        logger.DebugLog(`EXCEPTION: ${req.url}`, 'auth', 1)
        req.session = { isException: true }
        next()
        return
      }
      logger.DebugLog(`No user:${req.url}`, 'auth', 1)
      renderLogin(req, res)
      return
    }

    req.session = {
      user: user,
      sessionID: sessionID,
      isException: isException
    }

    logger.DebugLog(`ID #${user.id}: ${req.url}`, 'auth', 1)

    UpdateAccess(userDB, user, ip, sessionID)

    dbtools.Update(userDB, 'sessions', {
      lastAccess: utils.GetDateString()
    }, {
      id: sessionID
    })

    dbtools.Update(userDB, 'users', {
      lastIP: ip,
      lastAccess: utils.GetDateString()
    }, {
      id: user.id
    })

    next()
  }
}

function UpdateAccess (db, user, ip, sessionID) {
  const accesses = dbtools.Select(db, 'accesses', {
    userId: user.id,
    ip: ip
  })

  if (accesses.length === 0) {
    dbtools.Insert(db, 'accesses', {
      userID: user.id,
      ip: ip,
      sessionID: sessionID,
      date: utils.GetDateString()
    })
  }
}

function GetUserBySessionID (db, sessionID, req) {
  logger.DebugLog(`Getting user from db`, 'auth', 2)

  const session = dbtools.Select(db, 'sessions', {
    id: sessionID
  })[0]

  if (!session) {
    return
  }

  const user = dbtools.Select(db, 'users', {
    id: session.userID
  })[0]

  if (user) {
    return user
  }
}
