#!/bin/bash

if [ "$#" -ne "1" ]; then
  echo not enough params! please specify server root directory!
  exit 1
fi

export TERM=xterm-256color

# COLORS
NC='[0m'
# GRAY='[90m'
R='[91m'
G='[32m'
# YELLOW='[33m'
B='[94m'
P='[95m'
C='[36m'
sep="${P}--------------------------------------------------------------------------------<=$NC"

pcolumns () {
	mlength=35
	NUM=0;
	l=$(echo $"$1" | wc -l)
	sizeof2=$(echo $"$2" | wc -l)
	if [ "$l" -lt "$sizeof2" ]; then
		l=$(echo $"$2" | wc -l)
	fi
	for i in $( eval echo {0..$l} )
	do
		line=$(echo -ne $"$1" | cut -d$'\n' -f $(($NUM+1)))
		sliced="${line:0:$mlength}"
		last="${line: -3}"
		size=${#sliced}
		diff=$(($mlength-$size))

        if [ "$sliced" == "null" ]; then
          echo -n ''
        else
          echo -en "$sliced"
        fi
		if [ "$diff" -gt "0" ]; then
			diff=$((diff+2))
			for j in $( eval echo {0..$diff} )
			do
				echo -n " "
			done
		else
			echo -en "$last"
		fi

		sr=$(echo -ne $"$2" | cut -d$'\n' -f $(($NUM+1)))
        r=$"${sr:0:$mlength}"
        if [ "$r" == "null" ]; then
          echo ''
        else
          echo "$r"
        fi

		NUM=$((NUM+1))
	done
}

function jsonStats () {
  dateind=$(date '+%Y/%m/%d')
  b=$(cat "$1" | jq ".[\"$dateind\"]" | grep -ve '^{' | grep -ve '}$' | grep -ve '^\s*}' | sort)
  bc=$(cat "$1" | jq ".[\"$dateind\"] | length")
  b="\t${bc}\n${b}"

  dateind=$(date -d 'yesterday' '+%Y/%m/%d')
  a=$(cat "$1" | jq ".[\"$dateind\"]" | grep -ve '^{' | grep -ve '}$' | grep -ve '^\s*}' | sort)
  ac=$(cat "$1" | jq ".[\"$dateind\"] | length")
  a="\t${ac}\n${a}"

  pcolumns $"$a" $"$b" | sed -E \
    -e "s,/getVeteranPw,${C}&${NC},g" \
    -e "s,/getveteranpw,${C}&${NC},g" \
    -e "s,/pwRequest,${C}&${NC},g" \
    -e "s,/getpw,${C}&${NC},g" \
    -e "s,/avaiblePWS,${C}&${NC},g" \
    -e "s,/pwRequest,${C}&${NC},g" \
    -e "s,/login,${C}&${NC},g" \
    -e "s,/manual,${G}&${NC},g" \
    -e "s,/allQuestions,${G}&${NC},g" \
    -e "s,/subjectBrowser,${G}&${NC},g" \
    -e "s,/repos,${G}&${NC},g" \
    -e "s,/contribute,${G}&${NC},g" \
    -e "s,/reops,${G}&${NC},g" \
    -e "s,/feedback,${G}&${NC},g" \
    -e "s,/addQuestion,${G}&${NC},g" \
    -e "s,/dataCount,${G}&${NC},g" \
    -e "s,/menuClick,${G}&${NC},g" \
    -e "s,/allqr,${G}&${NC},g" \
    -e "s,/uploaddata,${G}&${NC},g" \
    -e "s,/legacy,${G}&${NC},g" \
    -e "s,/donate,${P}&${NC},g" \
    -e "s,/tiszai,${P}&${NC},g" \
    -e "s,/install,${P}&${NC},g" \
    -e "s,/irc,${P}&${NC},g" \
    -e "s,/postfeedback,${P}&${NC},g" \
    -e "s,/quickvote,${P}&${NC},g" \
    -e "s,/servergit,${P}&${NC},g" \
    -e "s,/scriptgit,${P}&${NC},g" \
    -e "s,/classesgit,${P}&${NC},g" \
    -e "s,/lred,${R}&${NC},g" \
    -e "s,/thanks,${R}&${NC},g" \
    -e "s,/isAdding,${B}&${NC},g" \
    -e "s,/ask,${B}&${NC},g"
}

function jsonStatsLength () {
  dateind=$(date '+%Y/%m/%d')
  bc=$(cat "$1" | jq ".[\"$dateind\"] | length")
  b="\t${bc}\n"

  dateind=$(date -d 'yesterday' '+%Y/%m/%d')
  ac=$(cat "$1" | jq ".[\"$dateind\"] | length")
  a="\t${ac}\n"

  pcolumns $"$a" $"$b"
}

echo -e "${G}Site requests yesterday / today:$NC"
jsonStats "${1}/stats/vstats"

echo -e "$sep"
echo -e "${G}Client ID test solving count yesterday / today:$NC"
jsonStatsLength "${1}/stats/idvstats"

echo -e "$sep"
echo -e "${G}User ID requests yesterday / today:$NC"
jsonStatsLength "${1}/stats/uvstats"

echo -e "$sep"
