#!/bin/bash

git pull
git submodule update --init --recursive

echo "Installing node modules for server"
npm install

echo "Making qmining page"
pushd modules/qmining/qmining-page/
npm install
npm run export
popd
ln -sf "$PWD/modules/qmining/qmining-page/out" "$PWD/modules/qmining/public"

echo "Making data editor page"
pushd modules/dataEditor/qmining-data-editor/
npm install
npm run export
popd
ln -sf "$PWD/modules/dataEditor/qmining-data-editor/out" "$PWD/modules/dataEditor/public"

echo "mkdir-ing/touching :3"

# TODO: make server create these itself
mkdir stats
touch nolog

#JSONS
echo '{}' > stats/stats
echo '{}' > stats/vstats
echo '{}' > stats/idstats
echo '{}' > stats/idvstats

touch qminingPublic/version
touch qminingPublic/motd

echo "wgetting data.json from frylabs..."
wget "http://qmining.frylabs.net/data.json" -O ./qminingPublic/data.json

if [ "$?" -ne "0" ]; then
  echo "Failed to wget data.json, please create it yourself!"
  echo "Now starting with empty data!"
  echo '{"Subjects":[],"version":"TESET","motd":"hai"}' > ./qminingPublic/data.json
fi

pushd utils
rm -v ../data/dbs/users.db
NS_SQL_DEBUG_LOG=true NS_LOGLEVEL=2 node dbSetup.js
popd

echo "Done!"
echo "npm start {loglevel}"
echo "To start server"
