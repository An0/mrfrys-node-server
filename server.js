/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */
const startHTTPS = true
const isRoot = process.getuid && process.getuid() === 0

const port = isRoot ? 80 : 8080
const httpsport = isRoot ? 443 : 5001

const express = require('express')
const vhost = require('vhost')
const logger = require('./utils/logger.js')
const utils = require('./utils/utils.js')
const http = require('http')
const https = require('https')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const uuidv4 = require('uuid/v4') // TODO: deprecated, but imports are not supported

const dbtools = require('./utils/dbtools.js')
const reqlogger = require('./middlewares/reqlogger.middleware.js')
const extraModulesFile = './extraModules.json'
const modulesFile = './modules.json'
const usersDBPath = 'data/dbs/users.db'

if (!utils.FileExists(usersDBPath)) {
  throw new Error('No user DB exists yet! please run utils/dbSetup.js first!')
}
const userDB = dbtools.GetDB(usersDBPath)

let modules = JSON.parse(utils.ReadFile(modulesFile))

logger.Load()

try {
  if (utils.FileExists(extraModulesFile)) {
    const extraModules = JSON.parse(utils.ReadFile(extraModulesFile))
    modules = {
      ...extraModules,
      ...modules
    }
  }
} catch (e) {
  logger.Log('Failed to read extra modules file')
  console.log(e)
}

// Setting up exits
// process.on('exit', () => exit('exit'))
process.on('SIGINT', () => exit('SIGINT'))
process.on('SIGTERM', () => exit('SIGTERM'))

function exit (reason) {
  console.log()
  logger.Log(`Exiting, reason: ${reason}`)
  Object.keys(modules).forEach((k, i) => {
    const x = modules[k]
    if (x.cleanup) {
      try {
        x.cleanup()
      } catch (e) {
        logger.Log(`Error in ${k} cleanup! Details in STDERR`, logger.GetColor('redbg'))
        console.error(e)
      }
    }
  })

  logger.Log('Closing Auth DB')
  userDB.close()

  process.exit()
}

const app = express()

if (!process.env.NS_DEVEL) {
  app.use(function (req, res, next) {
    if (req.secure) {
      next()
    } else {
      logger.DebugLog(`HTTPS ${req.method} redirect to: ${'https://' + req.headers.host + req.url}`, 'https', 1)
      if (req.method === 'POST') {
        res.redirect(307, 'https://' + req.headers.host + req.url)
      } else {
        res.redirect('https://' + req.headers.host + req.url)
      }
    }
  })
}
// https://github.com/expressjs/cors#configuration-options
app.use(cors({
  credentials: true,
  origin: true
  // origin: [ /\.frylabs\.net$/ ]
}))
const cookieSecret = uuidv4()
app.use(cookieParser(cookieSecret))
app.use(reqlogger({
  loggableKeywords: [
    'stable.user.js'
  ],
  loggableModules: [
    'dataeditor'
  ]
}))

Object.keys(modules).forEach(function (k, i) {
  let x = modules[k]
  try {
    let mod = require(x.path)
    logger.Log(`Loading ${mod.name} module`, logger.GetColor('yellow'))

    x.publicdirs.forEach((pdir) => {
      utils.CreatePath(pdir)
    })

    if (mod.setup) {
      mod.setup({
        url: 'https://' + x.urls[0],
        userDB: userDB,
        publicdirs: x.publicdirs,
        nextdir: x.nextdir
      })
    }

    const modApp = mod.getApp()
    x.app = modApp.app
    x.dailyAction = modApp.dailyAction
    x.cleanup = modApp.cleanup

    x.urls.forEach((url) => {
      app.use(vhost(url, x.app))
    })
  } catch (e) {
    console.log(e)
  }
})

const locLogFile = './stats/logs'
const allLogFile = '/nlogs/log'

// https://certbot.eff.org/
const privkeyFile = '/etc/letsencrypt/live/frylabs.net/privkey.pem'
const fullchainFile = '/etc/letsencrypt/live/frylabs.net/fullchain.pem'
const chainFile = '/etc/letsencrypt/live/frylabs.net/chain.pem'

var certsLoaded = false
if (startHTTPS && utils.FileExists(privkeyFile) && utils.FileExists(fullchainFile) && utils.FileExists(
  chainFile)) {
  try {
    const key = utils.ReadFile(privkeyFile)
    const cert = utils.ReadFile(fullchainFile)
    const ca = utils.ReadFile(chainFile)
    var certs = {
      key: key,
      cert: cert,
      ca: ca
    }
    certsLoaded = true
  } catch (e) {
    logger.Log('Error loading cert files!', logger.GetColor('redbg'))
    console.log(e)
  }
}

setLogTimer()
function setLogTimer () {
  const now = new Date()
  const night = new Date(
    now.getFullYear(),
    now.getMonth(),
    now.getDate() + 1,
    0,
    0,
    0
  )
  logger.DebugLog(`Next daily action: ${night}`, 'daily', 1)
  const msToMidnight = night.getTime() - now.getTime()
  logger.DebugLog(`msToMidnight: ${msToMidnight}`, 'daily', 1)
  logger.DebugLog(`Seconds To Midnight: ${msToMidnight / 1000}`, 'daily', 1)

  if (msToMidnight < 0) {
    logger.Log(`Error setting up Log Timer, msToMidnight is negative! (${msToMidnight})`, logger.GetColor('redbg'))
    return
  }

  setTimeout(function () {
    LogTimerAction()
    setLogTimer()
  }, msToMidnight)
}

function LogTimerAction () {
  logger.DebugLog(`Running Log Timer Action`, 'daily', 1)
  Object.keys(modules).forEach((k, i) => {
    const x = modules[k]
    logger.DebugLog(`Ckecking ${k}`, 'daily', 1)
    if (x.dailyAction) {
      try {
        logger.Log(`Running daily action of ${k}`)
        x.dailyAction()
      } catch (e) {
        logger.Log(`Error in ${k} daily action! Details in STDERR`, logger.GetColor('redbg'))
        console.err(e)
      }
    }
  })

  const line = '==================================================================================================================================================='
  logger.Log(line)
  utils.AppendToFile(line, locLogFile)
  utils.AppendToFile(line, allLogFile)
}

logger.Log('Node version: ' + process.version)
logger.Log('Current working directory: ' + process.cwd())
logger.Log('Listening on port: ' + port)
if (isRoot) {
  logger.Log('Running as root', logger.GetColor('red'))
}

const httpServer = http.createServer(app)
httpServer.listen(port)
if (certsLoaded) {
  const httpsServer = https.createServer(certs, app)
  httpsServer.listen(httpsport)
  logger.Log('Listening on port: ' + httpsport + ' (https)')
} else {
  logger.Log('Https not avaible')
}

// app.listen(port)
